# Author: Fabio Guerra <guerra.fabio.1998@gmail.com>
# Date: 11-08-2019

PRODUCT := a.out
SRCDIR := Src
BUILDDIR := Build
SRC := $(wildcard $(SOURCESDIR)/*.cpp)
OBJ := $(patsubst $(SOURCESDIR)/%.cpp,$(BUILDDIR)/%.o,$(SRC))
DEPS := deps.mk

CC := g++
GENDEPS := $(CC) -MM -MP $(SRC) | sed '/^$$/d; 2,$$s/^.*\.o:/\n&/; s/^.*\.o:/$(subst /,\/,$(subst .,\.,$(BUILDDIR)))\/&/' > $(DEPS)

# Development:
CPPFLAGS := -std=c++11 -Wall -Wextra -g3
# LDFLAGS :=
# Production:
# CPPFLAGS := -std=c++11 -O2
# LDFLAGS := --strip-all

# INCLUDES :=
# LDLIBS :=

.PHONY: cleanall clean

all: $(PRODUCT)

$(PRODUCT): $(OBJ)
	$(CC) $(LDFLAGS) $(LDLIBS) $^ -o $@
	$(GENDEPS)

$(OBJ): $(BUILDDIR)/%.o: $(SRCDIR)/%.cpp
	$(CC) -c $(CPPFLAGS) $(INCLUDES) $< -o $@

cleanall: clean
	-rm $(PRODUCT)

clean:
	-rm $(OBJ)
	-rm deps.mk

-include $(DEPS)
$(shell mkdir -p $(BUILDDIR))
